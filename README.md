# OpenML dataset: IMDB.drama

https://www.openml.org/d/273

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   Read, Jesse and Bifet, Albert and Pfahringer, Bernhard and Holmes, Geoff
**Source**: 
**Please cite**:   Read, Jesse & Bifet, Albert & Pfahringer, Bernhard & Holmes, Geoff. (2012). Batch-Incremental versus Instance-Incremental Learning in Dynamic and Evolving Data. 313-323. 10.1007/978-3-642-34156-4_29.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/273) of an [OpenML dataset](https://www.openml.org/d/273). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/273/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/273/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/273/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

